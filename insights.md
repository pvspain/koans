# RubyKoans::Insights

## Ranges

    a..b    # inclusive of both end points, a and b
    a...b   # Exclusive of upper end point, b

## Arrays

### Array-slicing

    array[array-length] => nil

    array[array-length,0] => []
    array[array-length,n] => []
    array[array-length,-n] => nil
    array[array-length+1,n] =>nil

Negative indices, starting at -1, index backwards from end of array

### Array appending

Push/Pop add/remove from _tail_ of array

Unshift/Shift add/remove from _head_ of array

The shovel symbol `<<` is an alias for `Array::push`

## Hashes

    Hash.new(obj)

`obj` is the default object to be returned for an unassigned hash value.
Note, this is not a _copy_ of `obj` to be returned, but `obj` itself.

    Hash.new{ |hash, key| hash[key] = default_value }

If a block is specified, it will  be called with the hash object and the
key, and should return the default value. It is the block's responsibility
to store the value in the hash if required.

## Strings

`"string"` - contents of string wrapped by double-quotes are interpreted by Ruby interpreter

`'string'` - contents of string wrapped by single-quotes are _not_ interpreted - a literal string

Ways to encapsulate text which may include single or double quotes:

* Use flexible quoting  `%(), %!!, %{}` - can extend over multiple lines

* Use a Here Doc

        long_string = <<EOS
        It was the best of times,
        It was the worst of times.
        EOS

    Newline character `\n` after opening EOS is _not_ part of string. Everything after shovel to end of line, including newline character, defines the Here Doc terminator.

### String-building

The concatenate operator `+` will leave the original strings unmodified

The shovel operator `<<` will modify the original strings and is therefore preferrable for string-building.

### Substrings

    string[first,length]    # Extract length chars beginning with first
    string[first..last]     # Extract chars from first to last
    string[first]           # Extract first char

## Symbols

* Symbols can contain spaces:

        :"cats and dogs"

* Symbols can be constructed from interpolated strings:

        :"cats #{value} dogs"

* `to_s` is called on interpolated symbols

        symbol = :cats
        string = "It is raining #{symbol} and dogs."

* Symbols can be compared
* Strings can be converted to symbols:

        string.to_sym

* Method names become symbols
* Names of constants become symbols

## Regular expressions: Regexp

* literal Regexp constructed with bounding forward slashes: `/pattern/`
* greedy - longest possible match returned
* leftmost/earliest match will be returned

        "match"[/a*/] == "" # regexp returns zero-length match at start of string, rather than "a" character

* unmatched regexp returns `nil`
* `/n?/` optional - matches 0 or 1 n
* `/n+/` matches 1 or more n
* `/n*/` matches 0 or more n
* `/./` matches any single, non-newline, character
* `/\s/` matches any single whitespace character: `[ \n\t]`
* `/\w/` matches any "word" character: `[a-zA-Z0-9_]`
* `/\A/` matches start of string (note: upper-case A)
* `/\z/` matches end of string (note: lower-case z)
* `/^start/` caret "^" matches the start of a line
* `/end$/` dollar matches the end of a line

* caret `^` negates grouping: `non-whitespace = [^ \n\t]`
* uppercase negates character class `s -> S, w -> W`

* parentheses group contents

        assert_equal "hahaha", "ahahaha"[/(ha)+/]

* matching groups can be accessed by number (1-based) or variable: $n

        assert_equal "James", "Gray, James"[/(\w+), (\w+)/, 2]
        assert_equal "Gray, James", "Name:  Gray, James"[/(\w+), (\w+)/]
        assert_equal "Gray", $1
        assert_equal "James", $2

* pipe character "|" ORs alternatives

* `scan` is like find all

        assert_equal ["one","two", "three"], "one two-three".scan(/\w+/)

* `sub` is ike find and replace one

        assert_equal "one t-three", "one two-three".sub(/(t\w*)/) { $1[0, 1] }

* `gsub` is like find and replace all

        assert_equal "one t-t", "one two-three".gsub(/(t\w*)/) { $1[0, 1] }

## Methods

* Methods can be called with or without parentheses around arguments. Parentheses are required to resolve ambiguity.
* Calling a method with the wrong number of arguments will raise a runtime error, `ArgumentError`.
* Methods can have default arguments:

          def method_with_defaults(a, b=:default_value)

* Methods can have variable arguments, passed in via "splat" notation:

          def method_with_var_args(*args)
            args
          end

### Private methods

* Declared within class, either:
    * after definition, with `private` keyword and method-name as symbol:

              def my_private_method
                "a secret"
              end
              private :my_private_method

    * following `private` keyword on its own - `private` applies to _all_ subsequent methods until another classifier encountered, e.g. `public` or `protected`

              private
              def my_private_method
                "a secret"
              end

* Called within class without receiver object
* Calling with receiver object (e.g. `self.private_method`), will raise a `NoMethodError`

## Scoping

* Scope can be _relative_ (to current scope) or _absolute_ (pathed from global scope).
* Scope character is `::`
* `::` is _global_ absolute scope

        ::C             # global constant 'C' - absolute scope
        C               # constant 'C' in current scope - relative scope
        ::AClass::C     # constant 'C' in class AClass - absolute scope
        AClass::C       # constant 'C' in class AClass within current scope - relative scope

## Constants

* Nested classes inherit constants from enclosing scope (including classes)
* Subclasses inherit constants from parent/super classes
* Constants within current lexical scope take precedence over same-named inherited constants (e.g. constants defined in nesting or current class)
* If a class is defined as nested, but _declared outside_ the scope of the nesting class, then inherited constants from super-classes take precedence over nesting-scoped  constants

## Control statements

        if boolean-expression [then]
            statement

* The result of an `if` statement can be assigned to a variable
* _Every_ statement in Ruby is an expression and can therefore be assigned to a variable
* `then` is optional (and typically unused) providing `boolean-expression` is followed by a new-line (or semi-colon) before the subsequent `statement`
* `if` can be used as a statement modifier

        statement if boolean-expression

* `unless` is equivalent to `if !`
* `break` returns control to statement following loop
* `next` short-circuits current iteration of loop
* `Break` and `next` can take an argument, which is the yielded value

        break i


## True and false

* `false` and `nil` are both _false_
* everything else is _true_

## Exceptions

* Class hierarchy:

        Object <- Exception <- StandardError <- RuntimeError

* Exception trapping context bounded by `begin` and `rescue`, with optional `ensure` clause that _always_ runs at end of block

        begin
           ...some code...
        rescue exception-type [ => exception-var ]
           ...code to handle exception
        [
        ensure
           ...code to _always_ run at end of block
        ]
        end

* Explicitly `raise` exception:

        raise exception-type "message"

## Iterators

Useful iterators defined in the Enumerable mixin:

* `map|collect` _always_ returns an Array, regardless of the type it is called upon
* `inject|reduce` accumulates the results of applying either:
    * A binary operator method name (as a symbol) passed as an argument
    * A block, plus an initial value for the accumulation

            a = (1..4)
            b = a.inject(:+)                       # b == 10
            c = a.inject(0) { |acc, i| acc + i }   # c == 10

* `select` returns an Array, a subset filtered by a boolean test described by a block

## Blocks

* Any method can take a _block_ ( aka closure in other languages )
* A block is bounded by braces `{}` (convention for one-liners) or `do end` (convention for multi-line)
* Block arguments are delimited by pipes `|`

        method { |a, b| a + b }

* The block is executed whenever `yield` is called within the method. Arguments to yield are passed as arguments to the block. The value of a yield expression is the evaluation of the block.

* As closures, blocks have access to everything in scope where the block is defined. Changes to variables within the block excution affect the value of those variables outside the block.square
square

* Blocks can be assigned to variables:
        
        square = lambda {|a| a*a }
      
        def method_with_explicit_block(&block)
          block.call(10)
        end

        method_with_explicit_block(&square)

* Blocks can be used to write the "meat" for sandwich code - where there is common initialisation and finalisation code, but the content changes. The content or "meat" can be injected into the shell code as a block:

        def file_sandwich(file_name)
          file = open(file_name)
          yield(file)
        ensure
          file.close if file
        end

        # Now we write:

        def count_lines2(file_name)
          file_sandwich(file_name) do |file|
            count = 0
            while file.gets
              count += 1
            end
            count
          end
        end

* Note that passing a block to `open` with the file object as the block argument achieves the same effect - finalisation:

        def count_lines3(file_name)
          open(file_name) do |file|
            count = 0
            while file.gets
              count += 1
            end
            count
          end
        end

## Classes

* Instances created with `new`

        class AClass 
          @colour = "Yellow"    # instance variable named colour
        end

        instance = AClass.new

* Refer to containing object within a method as `self`

* Accessors:

        class AClass
          attr_reader :name       # read-only attribute 'name'
          attr_accessor :age      # read-write attribute 'age'
        end

        instance = AClass.new
        instance.name
        instance.age = 42

* Construction arguments as arguments to method `initialize`. Arguments for `initialize` passed as arguments to `new`:

        class Dog
          attr_reader :name
          def initialize(initial_name)
            @name = initial_name
          end
        end

        fido = Dog.new "Fido"

### Inheritance

* Declaration of subclass:

    class ASubclass < ASuperClass

* Change method behaviour in subclass by re-implementing method

        class Dog
          attr_reader :name

          def initialize(name)
            @name = name
          end

          def bark
            "WOOF"
          end
        end

        class Chihuahua < Dog
          def wag
            :happy
          end

          def bark
            "yip"
          end
        end

* Invoke ancestor method within same-named subclass method by `super`

        class BullDog < Dog
          def bark
            super + ", GROWL"
          end
        end

## Modules

* Modules are declared similarly to classes:

        module Nameable
          def set_name(new_name)
            @name = new_name
          end

          def here
            :in_module
          end
        end

* Modules describe behaviour and data that can be `include`d in classes:

        class Dog
          include Nameable

          attr_reader :name
        end

* Module methods can affect instance variables within the object

        fido = Dog.new
        fido.set_name "Rover"

* Class methods override same-named module methods

## Class methods

* (Singleton) methods can defined on an object:

        fido = Dog.new
        def fido.wag
          :fidos_wag
        end

* (Singleton) methods can defined on a class, and are independent of instance methods:

        class Dog2
          def wag
            :instance_level_wag
          end
        end

        def Dog2.wag
          :class_level_wag
        end

        fido = Dog2.new
        assert_equal :instance_level_wag, fido.wag
        assert_equal :class_level_wag, Dog2.wag

* Class methods can be declared inside a class definition in any of three ways:

        class Demo
          def self.method
          end
     
          class << self
            def class_methods
            end
          end

          def Demo.another
          end
        end

* Accessing class methods from instances:

        demo = Demo.new
        demo.class.another

        